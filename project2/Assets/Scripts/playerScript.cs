﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerScript : MonoBehaviour {
    public Text ScoreText;
    public Text TimeText;

    public int deathCounter = 0;



    public Animator anim;

    Rigidbody2D rbody;
    bool canControl = true;

    public float speed;             //Floating point variable to store the player's movement speed.

    // Use this for initialization
    void Start() {



        rbody = gameObject.GetComponent<Rigidbody2D>();
        anim = gameObject.GetComponent<Animator>();



    }


    private void FixedUpdate()
    {
        rbody.velocity = new Vector2(Input.GetAxis("Horizontal") * 5, Input.GetAxis("Vertical") * 5);

        //float angle = GetAngle(Camera.main.ScreenToWorldPoint(Input.mousePosition));

        float rotFraction = 4 * Time.deltaTime;
    }

    // Update is called once per frame
    void Update () {
       // DontDestroyOnLoad(ScoreText);
        DontDestroyOnLoad(TimeText);

        float force = 150 * Time.deltaTime;




        if (Input.GetKey(KeyCode.RightArrow))
        {
           // gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(force, 0));
            anim.Play("playerRight");

            print("You pressed right");

        }

        else if (Input.GetKey(KeyCode.LeftArrow))
        {
           // gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-force, 0));
            anim.Play("playerLeft");

            print("You pressed left");

        }

        else if (Input.GetKey(KeyCode.UpArrow))
        {
           // gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, force));
            anim.Play("playerUp");

            print("You pressed up");

        }

        else if (Input.GetKey(KeyCode.DownArrow))
        {

           // gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -force));
            anim.Play("playerIdle");

            print("You pressed down");

        }



        float time = Time.realtimeSinceStartup;

        TimeText.text = "Time - " + Mathf.Floor(time);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        // print("You hit something");

        //collisionCounter++;

        if (collision.gameObject.name == "Enemy3")
        {
            print("Thats and enemy, restart");
            Initiate.Fade("level3", Color.black, 1.0f);

            deathCounter = deathCounter + 1;

            ScoreText.text = "Deaths - " + deathCounter;




        }

        if (collision.gameObject.name == "Enemy2")
        {
            print("Thats and enemy, restart");
            Initiate.Fade("level2", Color.black, 1.0f);

            deathCounter = deathCounter + 1;

            ScoreText.text = "Deaths - " + deathCounter;




        }

        if (collision.gameObject.name == "Enemy3")
        {
            print("Thats and enemy, restart");
            Initiate.Fade("level3", Color.black, 1.0f);

            deathCounter = deathCounter + 1;

            ScoreText.text = "Deaths - " + deathCounter;




        }

        if (collision.gameObject.name == "Enemy1")
        {
            print("Thats and enemy, restart");
            Initiate.Fade("level1", Color.black, 1.0f);

            deathCounter = deathCounter + 1;

            ScoreText.text = "Deaths - " + deathCounter;




        }


        if (collision.gameObject.name == "Enemy4")
        {
            print("Thats and enemy, restart");
            Initiate.Fade("level4", Color.black, 1.0f);

            deathCounter = deathCounter + 1;

            ScoreText.text = "Deaths - " + deathCounter;




        }

        if (collision.gameObject.name == "teleportingTreeTo4")
        {
            print("Thats and enemy, restart");
            Initiate.Fade("level4", Color.black, 1.0f);






        }

        if (collision.gameObject.name == "teleportingTreeTo3")
        {
            print("Thats and enemy, restart");
            Initiate.Fade("level3", Color.black, 1.0f);






        }

        if (collision.gameObject.name == "teleportingTreeStart")
        {
            print("Thats and enemy, restart");
            Initiate.Fade("level1", Color.black, 1.0f);






        }

        if (collision.gameObject.name == "teleportingTreeTo2")
        {
            print("Thats and enemy, restart");
            Initiate.Fade("level2", Color.black, 1.0f);






        }

        if (collision.gameObject.name == "teleportingTreeWin")
        {
            print("Thats and enemy, restart");
            Initiate.Fade("level1", Color.black, 1.0f);






        }

    }


}


